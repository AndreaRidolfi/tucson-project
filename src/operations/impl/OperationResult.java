/**
 * OperationResult.java
 */
package operations.impl;

import java.util.ArrayList;
import java.util.List;

import operations.api.IOperation;
import operations.api.IOperationResult;
import alice.logictuple.LogicTuple;
import alice.tuplecentre.api.Tuple;

/**
 * @author Andrea Ridolfi (mailto: andrea.ridolfi5@studio.unibo.it) on
 *         16/mag/2014
 * 
 */
public class OperationResult implements IOperationResult {
    /** serialVersionUID **/
    private static final long serialVersionUID = 1L;
    /*
     * (non-Javadoc)
     * @see operations.api.IOperationResult#getError()
     */
    private Tuple error, result;
    private boolean iscompleted;
    private boolean isfailed;
    private List<Tuple> resultList;
    private long time;
    private IOperation op;

    /**
     * @param op2 the operation associated to the result
     * Constructor
     */
    public OperationResult(final IOperation op2) {
        this.setOperation(op2);
        this.iscompleted = false;
        this.isfailed = false;
        this.error = new LogicTuple();
        this.result = new LogicTuple();
        this.resultList = new ArrayList<Tuple>();
        this.time = 0;
        
    }

    /*
     * (non-Javadoc)
     * @see operations.api.IOperationResult#getError()
     */
    @Override
    public Tuple getError() {
        return this.error;
    }

    /*
     * (non-Javadoc)
     * @see operations.api.IOperationResult#getResult()
     */
    @Override
    public Tuple getResult() {
        return this.result;
    }

    /*
     * (non-Javadoc)
     * @see operations.api.IOperationResult#getResultList()
     */
    @Override
    public List<Tuple> getResultList() {
        return this.resultList;
    }

    /*
     * (non-Javadoc)
     * @see operations.api.IOperationResult#getTimeExecution()
     */
    @Override
    public long getTimeExecution() {
        return this.time;
    }

    /*
     * (non-Javadoc)
     * @see operations.api.IOperationResult#isCompleted()
     */
    @Override
    public boolean isCompleted() {
        return this.iscompleted;
    }

    /*
     * (non-Javadoc)
     * @see operations.api.IOperationResult#isFailed()
     */
    @Override
    public boolean isFailed() {
        return this.isfailed;
    }

    /*
     * (non-Javadoc)
     * @see operations.api.IOperationResult#setError(java.lang.String)
     */
    @Override
    public void setError(final Tuple s) {
        this.isfailed = true;
        this.iscompleted = true;
        this.error = s;
    }

    /*
     * (non-Javadoc)
     * @see operations.api.IOperationResult#setResult()
     */
    @Override
    public void setResult(final Tuple s) {
        this.iscompleted = true;
        this.result = s;
    }

    /*
     * (non-Javadoc)
     * @see operations.api.IOperationResult#setResultList()
     */
    @Override
    public void setResultList(final List<Tuple> tuple) {
        this.iscompleted = true;
        this.resultList = tuple;
    }

    /*
     * (non-Javadoc)
     * @see operations.api.IOperationResult#setTimeExecution(long)
     */
    @Override
    public void setTimeExecution(final long t) {
        this.time = System.currentTimeMillis() - t;
    }

    /* (non-Javadoc)
     * @see operations.api.IOperationResult#setOperation(operations.api.IOperation)
     */
    public void setOperation(final IOperation op2) {
        this.op = op2;
    }

    /* (non-Javadoc)
     * @see operations.api.IOperationResult#getOperation()
     */
    public IOperation getOperation() {
        return this.op;
    }
}
