package tuplespace.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import logging.MyLogger;
import operations.api.IOperation;
import operations.api.IOperationResult;
import tuplespace.api.ITupleCentre;
import tuplespace.api.ITupleCentreOperations;
import tuplespace.buffers.TaskBlock;
import alice.logictuple.LogicTuple;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tuplecentre.api.Tuple;
import alice.tuplecentre.api.exceptions.InvalidOperationException;

/*
 * TODO LinkedList > List, within HashMap ArrayList > List
 */

/**
 * Implementation of the TupleCentre class
 * 
 * [TEMPORARY] For now we use the basic LogicTuple already implemented in the
 * current TuCSoN (see documentation) To be changed based on what other teams
 * do.
 * 
 * @author Andrea (mailto: andrea.ridolfi5@studio.unibo.it) on Apr 3, 2014
 * 
 */
public class TupleCentre implements ITupleCentre, ITupleCentreOperations {
    private final List<TaskBlock> pendingOp;
    private final String tci;
    private final TupleCentreResultManager tcrm;
    private HashMap<String, ArrayList<Tuple>> tupleMap;
    private int tuplenr;

    /**
     * 
     * @param tci2
     *            the name of the tuplecentre
     * @param tcrm2
     *            the tuplecentreresultmanager
     */
    public TupleCentre(final TucsonTupleCentreId tci2,
            final TupleCentreResultManager tcrm2) {
        this.tuplenr = 0;
        this.tcrm = tcrm2;
        this.tci = tci2.getName();
        this.pendingOp = new LinkedList<>();
        this.tupleMap = new HashMap<String, ArrayList<Tuple>>();
        MyLogger.log("[TupleCentre]: Created new TupleCentre: "
                + tci2.getName());
    }

    /**
     * shuts down the tupleCentre
     */
    public void shutDown() {
        // Here we should save the state of the tuplecentre
        MyLogger.log("[CENTRE" + this.tci + "]: SHUTTING DOWN");
    }

    /**
     * Check the pending operation list and try to execute all the operation we
     * find. If one succede, we break the cycle and restart until no operation
     * from the list can be executed, then we go on serving new requests.
     */
    private void checkPendingOps() {
        boolean nonecompleted = true;
        for (int i = 0; i < this.pendingOp.size(); i++) {
            final IOperation op = this.pendingOp.get(i).getOperation();
            final IOperationResult opres = op.execute(this);
            if (!opres.isFailed()) {
                final TaskBlock task = this.pendingOp.remove(i);
                op.setCompleted();
                this.tcrm.insertResult(opres, op, task.getAgent(),
                        task.getBlockId());
                nonecompleted = false;
                break;
            }
        }
        if (!nonecompleted) {
            this.checkPendingOps();
        }
    }

    /**
     * The doOperation is Synchronized, since we can perform only a single
     * operation at a time in the tuplecentre. This is because, even if we
     * theoretically could execute read operation concurrently, with ReSpecT
     * reaction we could trigger some other operation which can modify the
     * tupleSpace. Thus the sequentialization of all the operation performed in
     * the space. Also, the operation are inserted in a list with the FIFO
     * policy.
     * 
     * @param task
     *            the task we want to perform
     */
    @Override
    public synchronized void doOperation(final TaskBlock task) {
        final IOperation op = task.getOperation();
        final IOperationResult opres = op.execute(this);
        if (opres.isFailed()) {
            op.setPending();
            ((LinkedList<TaskBlock>) this.pendingOp).addFirst(task);
        } else {
            op.setCompleted();
            this.tcrm.insertResult(opres, op, task.getAgent(),
                    task.getBlockId());
            this.checkPendingOps();
        }
    }

    /*
     * (non-Javadoc)
     * @see tuplespace.api.ITupleCentreOperations#getTupleList()
     */
    @Override
    public List<Tuple> getTupleList() {
        // private HashMap<String, ArrayList<Tuple>> tupleMap =
        final ArrayList<Tuple> resultlist = new ArrayList<>();
        for (final Entry<String, ArrayList<Tuple>> entry : this.tupleMap
                .entrySet()) {
            resultlist.addAll(entry.getValue());
        }
        return resultlist;
    }

    /*
     * (non-Javadoc)
     * @see tuplespace.api.ITupleCentreOperations#addTuple()
     */
    @Override
    public Tuple addTuple(final Tuple tuple2) throws InvalidOperationException {
        final LogicTuple tuple = (LogicTuple) tuple2;
        ArrayList<Tuple> tupleList;
        tupleList = this.tupleMap.get(tuple.getName());
        // If the tuple is not existent, we insert it in the hashmap and create
        // and arraylist correspondent
        if (tupleList == null) {
            tupleList = new ArrayList<>();
            tupleList.add(tuple);
            this.tupleMap.put(tuple.getName(), tupleList);
        } else {
            // The tuple is yet in the centre. In this case we retrieve the
            // arraylist correspondent and insert in it the new tuple
            tupleList.add(tuple);
        }
        this.tuplenr++;
        MyLogger.debug("[CENTRE] Added tuple " + tuple2.toString() + " #"
                + this.tuplenr);
        return tuple2;
    }

    /*
     * (non-Javadoc)
     * @see tuplespace.api.ITupleCentreOperations#readTuple()
     */
    @Override
    public Tuple readTuple(final Tuple tuple2) throws InvalidOperationException {
        final LogicTuple tuple = (LogicTuple) tuple2;
        ArrayList<Tuple> tupleList;
        tupleList = this.tupleMap.get(tuple.getName());
        // If the tuple is not existent, we return an empty tuple.
        if (tupleList == null) {
            return new LogicTuple("");
        }
        // The tuple is yet in the centre. In this case we retrieve the
        // arraylist correspondent and insert in it the new tuple
        for (final Tuple t : tupleList) {
            if (tuple.match(t)) {
                MyLogger.debug("[CENTRE] Read tuple " + t.toString());
                return t;
            }
        }
        return new LogicTuple("");
    }

    /*
     * (non-Javadoc)
     * @see tuplespace.api.ITupleCentreOperations#removeTuple()
     */
    @Override
    public Tuple removeTuple(final Tuple tuple2)
            throws InvalidOperationException {
        final LogicTuple tuple = (LogicTuple) tuple2;
        ArrayList<Tuple> tupleList;
        tupleList = this.tupleMap.get(tuple.getName());
        // If the tuple is not existent, we return an empty Tuple
        if (tupleList == null) {
            return new LogicTuple("");
        }
        // The tuple is yet in the centre. In this case we retrieve the
        // arraylist correspondent and insert in it the new tuple
        for (final Tuple t : tupleList) {
            if (tuple.match(t)) {
                tupleList.remove(t);
                MyLogger.debug("[CENTRE] Removed tuple " + t.toString());
                this.tuplenr--;
                return t;
            }
        }
        return new LogicTuple("");
    }

    /*
     * (non-Javadoc)
     * @see tuplespace.api.ITupleCentreOperations#clearTupleSpace()
     */
    @Override
    public void clearTupleSpace() {
        this.tupleMap = new HashMap<>();
        this.tuplenr = 0;
    }
}
