/**
 * TaskBuffer.java
 */
package tuplespace.buffers;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Andrea Ridolfi (mailto: andrea.ridolfi5@studio.unibo.it) on
 *         29/mag/2014
 * 
 */
public class TaskBuffer {
    private boolean available;
    private final List<TaskBlock> buffer;
    private final Condition isAvail;
    private final Lock mutex;

    /**
     * 
     */
    public TaskBuffer() {
        this.buffer = new LinkedList<>();
        this.mutex = new ReentrantLock();
        this.isAvail = this.mutex.newCondition();
        this.available = false;
    }

    /**
     * The first task inserted will be removed, FIFO
     * 
     * @return the next task we are going to perform
     */
    public TaskBlock getTask() {
        try {
            this.mutex.lock();
            while (!this.available) {
                try {
                    this.isAvail.await();
                } catch (final InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
            final TaskBlock result = ((LinkedList<TaskBlock>) this.buffer).removeFirst();
            if (this.buffer.isEmpty()) {
                this.available = false;
            }
            return result;
        } finally {
            this.mutex.unlock();
        }
    }

    /**
     * This methos adds a task in the buffer
     * 
     * @param task
     *            task we want to insert in the buffer
     */
    public void putTask(final TaskBlock task) {
        try {
            this.mutex.lock();
            this.buffer.add(task);
            this.available = true;
            this.isAvail.signal();
        } finally {
            this.mutex.unlock();
        }
    }
}
