/**
 * Worker.java
 */
package tuplespace.workers;

import tuplespace.api.ITupleCentre;
import tuplespace.buffers.TaskBlock;
import tuplespace.buffers.TaskBuffer;

/**
 * @author Andrea Ridolfi (mailto: andrea.ridolfi5@studio.unibo.it) on 29/mag/2014
 *
 */
public class OpExecutor extends Thread {

    private TaskBuffer taskbuffer;
    
    
    public OpExecutor(final TaskBuffer buffer) {
        this.taskbuffer = buffer;
    }
    
    
    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
        
        while (true) {
            TaskBlock task = this.taskbuffer.getTask();
            ITupleCentre tc = task.getTc();
            tc.doOperation(task);
            
            
        }
    }

}
