/**
 * HelloWorlds.java
 */
package nonJUnit.tester;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import nonJUnit.external.ExternalLayer;
import nonJUnit.gui.MainGui;
import nonJUnit.tasks.WriterTask;
import operations.api.IOperation;
import operations.api.IOperationResult;
import operations.impl.OpGet;
import tuplespace.exceptions.TupleCentreNotFoundException;
import alice.logictuple.LogicTuple;
import alice.tucson.api.TucsonAgentId;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tuplecentre.api.Tuple;
import alice.tuplecentre.api.exceptions.InvalidOperationException;

/**
 * @author Andrea Ridolfi (mailto: andrea.ridolfi5@studio.unibo.it) on
 *         29/mag/2014
 * 
 */
public final class MainTester {

    private ExternalLayer nodo;
    private MainGui gui;
    private ExecutorService executor;

    /**
     * 
     */
    private MainTester() {

        nodo = new ExternalLayer();
        gui = new MainGui(this);

    }

    /**
     * New Test to try the new TuCSoN implementation
     * 
     * @param args
     *            main args
     */
    public static void main(final String[] args) {

        new MainTester();

    }

    /**
     * 
     */
    public void startFirstTest() {
        try {

            executor = Executors.newFixedThreadPool(5);

            long t0 = System.nanoTime();

            //1000 Task, each one generates 1000 tuple
            for (int i = 1; i < 101; i++) {
                TucsonAgentId aid = new TucsonAgentId("task" + i);
                WriterTask t = new WriterTask(nodo, aid, gui);
                executor.execute(t);
                gui.UpdateText(aid + "started");
            }

            // Wait for their completion
            executor.shutdown();
            executor.awaitTermination(3, TimeUnit.MINUTES);

            long t1 = System.nanoTime();

            // Writining elapsed time at the end of the execution
            gui.UpdateText("Time elapsed: " + (t1 - t0) * 0.000001);
        } catch (TucsonInvalidAgentIdException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    /**
     * 
     */
    public void startSecondTest() {
        try {
            TucsonAgentId aid = new TucsonAgentId("secondtest");
            TucsonTupleCentreId tci = new TucsonTupleCentreId("andrea");
            IOperation op = new OpGet();
            IOperationResult res = this.nodo.getResult(this.nodo.doOp(op, aid, tci));
            List<Tuple> list = res.getResultList();
            
            int total = 0;
            for (Tuple entry : list){
                LogicTuple tuple = (LogicTuple)entry;
                total = total + tuple.getArg(0).intValue();
            }
            this.gui.UpdateText("Andrea ha " + total);
            
            tci = new TucsonTupleCentreId("massimo");
            res = this.nodo.getResult(this.nodo.doOp(op, aid, tci));
            list = res.getResultList();
            
            total = 0;
            for (Tuple entry : list){
                LogicTuple tuple = (LogicTuple)entry;
                total = total + tuple.getArg(0).intValue();
            }
            this.gui.UpdateText("Massimo ha " + total);
            
            tci = new TucsonTupleCentreId("marco");
            res = this.nodo.getResult(this.nodo.doOp(op, aid, tci));
            list = res.getResultList();
            
            total = 0;
            for (Tuple entry : list){
                LogicTuple tuple = (LogicTuple)entry;
                total = total + tuple.getArg(0).intValue();
            }
            this.gui.UpdateText("MarcoPari ha " + total);
            
            
        } catch (TucsonInvalidAgentIdException e) {
            e.printStackTrace();
        } catch (TucsonInvalidTupleCentreIdException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (TupleCentreNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvalidOperationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        

    }

    /**
     * 
     */
    public void startThirdTest() {
        // TODO Auto-generated method stub

    }

    /**
     * 
     */
    public void startForthTest() {
        // TODO Auto-generated method stub

    }
}
