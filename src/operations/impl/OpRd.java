package operations.impl;

import java.util.List;

import operations.api.AbstractOperation;
import operations.api.IOperationResult;
import tuplespace.api.ITupleCentreOperations;
import alice.logictuple.LogicTuple;
import alice.tuplecentre.api.Tuple;
import alice.tuplecentre.api.exceptions.InvalidOperationException;

/**
 * 
 * looks for a tuple matching TupleTemplate in the target tuple space; if a
 * matching Tuple is found when the operation is first served, the execution
 * succeeds by returning Tuple; otherwise, the execution is suspended, to be
 * resumed and successfully completed when a matching Tuple is finally found on
 * the target tuple space, and returned
 * 
 * @author Andrea (mailto: andrea.ridolfi5@studio.unibo.it) on Apr 3, 2014
 * 
 */
public class OpRd extends AbstractOperation {
    /** serialVersionUID **/
    private static final long serialVersionUID = 1L;
    private final Tuple tuple;

    // THIS MUST BE CHANGED IN A TUPLETEMPLATE!
    /**
     * 
     * We want to read a tuple and NOT consuming it
     * 
     * @param tuple1
     *            the tuple we want to look for
     */
    public OpRd(final Tuple tuple1) {
        super();
        this.tuple = tuple1;
    }

    /**
     * @param tc
     *            the tuplecentre where we want to perform the operation
     * @return the object return
     */
    @Override
    public IOperationResult execute(final ITupleCentreOperations tc) {
        try {

            Tuple result = tc.readTuple(this.tuple);
            if (((LogicTuple) result).getName().equals("")) {
                final OperationResult opres = new OperationResult(this);
                opres.setError(this.tuple);
                return opres;
            }
            final OperationResult opres = new OperationResult(this);
            opres.setResult(result);
            return opres;

        } catch (final InvalidOperationException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Tuple getTuple() {
        return this.tuple;
    }

    /*
     * (non-Javadoc)
     * @see operations.api.IOperation#getTupleList()
     */
    @Override
    public List<Tuple> getTupleList() {
        return null;
    }
}
