/**
 * TaskBlock.java
 */
package tuplespace.buffers;

import java.io.Serializable;

import operations.api.IOperation;
import tuplespace.api.ITupleCentre;
import alice.tucson.api.TucsonAgentId;

/**
 * @author Andrea Ridolfi (mailto: andrea.ridolfi5@studio.unibo.it) on
 *         29/mag/2014
 * 
 */
public class TaskBlock implements Serializable {
    /** serialVersionUID **/
    private static final long serialVersionUID = 1L;
    private final TucsonAgentId agentId;
    private final IOperation op;
    private final int opId;
    private final ITupleCentre tc;

    /**
     * @param op2
     *            the operation we want to perform on the tid
     * @param aid
     *            the agent who requested the operation
     * @param tc2
     *            the tuple centre where we want to perform the operation
     * @param opId2
     *            the id of the operation
     */
    public TaskBlock(final IOperation op2, final TucsonAgentId aid,
            final ITupleCentre tc2, final int opId2) {
        this.op = op2;
        this.agentId = aid;
        this.tc = tc2;
        this.opId = opId2;
    }

    /**
     * 
     * @return the agent who requested the operetion
     */
    public TucsonAgentId getAgent() {
        return this.agentId;
    }

    /**
     * 
     * @return the unique identifier of the operation
     */
    public int getBlockId() {
        return this.opId;
    }

    /**
     * 
     * @return the operation to be performed
     */
    public IOperation getOperation() {
        return this.op;
    }

    /**
     * 
     * @return the tuplecentre where we want to perform the operation
     */
    public ITupleCentre getTc() {
        return this.tc;
    }
}
