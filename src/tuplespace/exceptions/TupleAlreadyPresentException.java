package tuplespace.exceptions;

/**
 * 
 * @author Andrea (mailto: andrea.ridolfi5@studio.unibo.it) on Apr 3, 2014
 * 
 */
public class TupleAlreadyPresentException extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
}
