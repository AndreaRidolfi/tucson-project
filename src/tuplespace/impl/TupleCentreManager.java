package tuplespace.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import logging.MyLogger;
import nonJUnit.external.IExternalLayer;
import operations.api.IOperation;
import operations.api.IOperationResult;
import operations.impl.OperationResult;
import tuplespace.api.ITupleCentre;
import tuplespace.api.ITupleCentreManager;
import tuplespace.buffers.TaskBlock;
import tuplespace.buffers.TaskBuffer;
import tuplespace.exceptions.TupleCentreAlreadyExistsException;
import tuplespace.exceptions.TupleCentreNotFoundException;
import tuplespace.workers.OpExecutor;
import alice.tucson.api.TucsonAgentId;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;

/*
 * TODO Why not using the Executors framework? Maybe with Futures?
 */
/**
 * TupleSpace has an HashMap of all the centres he has He also own a List of all
 * the operation still executing, to retrieve at any given moment their
 * execution status Notice that the getCurrStatus isn't synchronized, thus it
 * can be executed while the result is being generated.
 * 
 * @author Andrea (mailto: andrea.ridolfi5@studio.unibo.it) on Apr 3, 2014
 * 
 */
public class TupleCentreManager implements ITupleCentreManager {
    private final TaskBuffer buffer;
    // Key of the HashMap is the name of the centre itself
    private final HashMap<String, ITupleCentre> centres;
    
    private final TupleCentreResultManager resultManager;
    
    private final HashMap<Integer, IOperation> pendingOperations;

    /**
     * constructor We initialize the hashmap and create the default tuplecentre
     * 
     * @param layer
     *            external layer
     */
    public TupleCentreManager(final IExternalLayer layer) {
        MyLogger.log("[TupleCentreManager]: Set Up");
        this.centres = new HashMap<>();
        this.resultManager = new TupleCentreResultManager(layer);
        this.buffer = new TaskBuffer();
        this.pendingOperations = new HashMap<Integer, IOperation>();
        try {
            setUpTupleCentre(new TucsonTupleCentreId("default"));
        } catch (TupleCentreAlreadyExistsException e) {
            e.printStackTrace();
        } catch (TucsonInvalidTupleCentreIdException e) {
            e.printStackTrace();
        }
        // Number of thread is ncore/2, if we have 1 core we create just one.
        int nthread = Runtime.getRuntime().availableProcessors() / 2;
        if (nthread == 0) {
            nthread++;
        }
        for (int i = 0; i < nthread; i++) {
            OpExecutor thread = new OpExecutor(this.buffer);
            thread.start();
        }
    }

    /*
     * (non-Javadoc)
     * @see tuplespace.api.ITupleCentreManager#doOperation() the unique
     * operation identifier
     */
    @Override
    public synchronized void doOperation(final IOperation op,
            final TucsonAgentId aid, final TucsonTupleCentreId tci,
            final int opId) {
        this.pendingOperations.put(opId, op);
        ITupleCentre tc = this.centres.get(tci.getName());
        if (tc == null) {
            try {
                this.setUpTupleCentre(new TucsonTupleCentreId(tci.getName()));
                tc = this.centres.get(tci.getName());
            } catch (final TupleCentreAlreadyExistsException e) {
                e.printStackTrace();
            } catch (final TucsonInvalidTupleCentreIdException e) {
                e.printStackTrace();
            }
        }
        final TaskBlock task = new TaskBlock(op, aid, tc, opId);
        this.buffer.putTask(task);
    }

    @Override
    public IOperationResult getCurrStatus(final int opId) {
        IOperation op = this.pendingOperations.get(opId);
        if (op == null) {
            //TODO this should be changed with a meaningful method in the case we don't find the operation 
            return null;
        }
        IOperationResult result = new OperationResult(op);
        return result;
    }

    /*
     * (non-Javadoc)
     * @see tuplespace.api.ITupleCentreManager#setUpTupleCentre()
     */
    @Override
    public void setUpTupleCentre(final TucsonTupleCentreId tci)
            throws TupleCentreAlreadyExistsException {
        if (this.centres.get(tci.getName()) == null) {
            MyLogger.log("[TupleCentreManager]: new tuplecentre created -> "
                    + tci.getName());
            this.centres.put(tci.getName(), new TupleCentre(tci,
                    this.resultManager));
        } else {
            throw new TupleCentreAlreadyExistsException();
        }
    }

    /*
     * (non-Javadoc)
     * @see tuplespace.api.ITupleCentreManager#shutDownTupleCentre()
     */
    @Override
    public void shutDownTupleCentre(final TucsonTupleCentreId tci)
            throws TupleCentreNotFoundException {
        final ITupleCentre tc = this.centres.get(tci.getName());
        if (tc == null) {
            throw new TupleCentreNotFoundException();
        }
        tc.shutDown();
        this.centres.remove(tci);
        MyLogger.log("[TupleCentreManager]: tuplecentre shutdown -> "
                + tci.getName());
    }

}
