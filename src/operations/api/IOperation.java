package operations.api;

import java.io.Serializable;
import java.util.List;

import tuplespace.api.ITupleCentreOperations;
import alice.tuplecentre.api.Tuple;

/**
 * The basic interface of a TuCSoN operation
 * 
 * @author Andrea (mailto: andrea.ridolfi5@studio.unibo.it) on Apr 3, 2014
 * 
 */
public interface IOperation extends Serializable {
    /**
     * 
     * @param tc
     *            tuplecentre where we must perform the operation
     * @return the object result
     */
    IOperationResult execute(final ITupleCentreOperations tc);

    /**
     * 
     * @return the tuple of the operation
     */
    Tuple getTuple();

    /**
     * @return an arraylist containing the result tuples of the operation
     */
    List<Tuple> getTupleList();

    /**
     * 
     * @return wheter the operation is completed or not
     */
    boolean isCompleted();

    /**
     * called when the operation is completed and the correspondent
     * OperationResult object has been created
     */
    void setCompleted();

    /**
     * 
     * @return wheter the operation is pending or not
     */
    boolean isPending();

    /**
     * called when the operation in pending in the tuplecentre
     */
    void setPending();

}
