/**
 * IOperationResult.java
 */
package operations.api;

import java.io.Serializable;
import java.util.List;

import alice.tuplecentre.api.Tuple;

/**
 * @author Andrea Ridolfi (mailto: andrea.ridolfi5@studio.unibo.it) on
 *         16/mag/2014
 * 
 */
public interface IOperationResult extends Serializable {
    /**
     * 
     * @return a Tuple containing the tuple of the operation failed
     */
    Tuple getError();

    /**
     * 
     * @return a Tuple with the result of the operation
     */
    Tuple getResult();

    /**
     * 
     * @return an arraylist containing a list the result
     */
    List<Tuple> getResultList();

    /**
     * 
     * @return how much time the operation last between call and execution
     */
    long getTimeExecution();

    /**
     * 
     * @return wheter the operation has been executed or not
     */
    boolean isCompleted();

    /**
     * 
     * @return wheter the operation has failed or not
     */
    boolean isFailed();

    /**
     * 
     * @param s
     *            the error message we set
     */
    void setError(Tuple s);

    /**
     * 
     * @param s
     *            the result message we set
     */
    void setResult(Tuple s);

    /**
     * 
     * @param tuplelist
     *            list of tuple result
     */
    void setResultList(List<Tuple> tuplelist);

    /**
     * 
     * @param time
     *            the time we started the operation
     */
    void setTimeExecution(long time);
    
    /**
     * 
     * @param op 
     *          the operation of the result
     */
    void setOperation(IOperation op);
    
    /**
     * 
     * @return the operation
     */
    IOperation getOperation();
    
    
}
