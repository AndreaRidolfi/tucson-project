/**
 * ExternalLayer.java
 */
package nonJUnit.external;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import operations.api.IOperation;
import operations.api.IOperationResult;
import tuplespace.exceptions.TupleCentreNotFoundException;
import tuplespace.impl.TupleCentreManager;
import logging.MyLogger;
import alice.tucson.api.TucsonAgentId;
import alice.tucson.api.TucsonTupleCentreId;

/**
 * This is based on our mock implementation of the NodeLayer, which will be
 * created on top of ours Note that, even if all the data structures works,
 * there are a lot of thing that need to be changed. For istance when we call
 * the getResult() method we actually can not specify which operation's result
 * we want to retrieve, but the layer decides on its own based on the first
 * occurence it can find.
 * 
 * @author Andrea Ridolfi (mailto: andrea.ridolfi5@studio.unibo.it) on Jul 31,
 *         2014
 * 
 */
public class ExternalLayer implements IExternalLayer {

    private Lock mutex;
    private TupleCentreManager tcm;
    private HashMap<Integer, IOperationResult> results;
    private HashMap<Integer, TucsonAgentId> mapping;
    private HashMap<Integer, Condition> available;
    private int opId;

    public ExternalLayer() {
        this.tcm = new TupleCentreManager(this);

        this.results = new HashMap<Integer, IOperationResult>();
        this.available = new HashMap<Integer, Condition>();
        this.mapping = new HashMap<Integer, TucsonAgentId>();
        this.mutex = new ReentrantLock();

        this.opId = 1;
    }

    /*
     * (non-Javadoc)
     * @see
     * test.external.NodeLayer#notifyResult(operations.api.IOperationResult,
     * alice.tucson.api.TucsonAgentId, int)
     */
    public void notifyResult(final IOperationResult operationResult,
            final TucsonAgentId aid, final int opid) {
        try {
            this.mutex.lock();
            MyLogger.debug("Result is ready for operation " + opid);
            this.results.put(opid, operationResult);
            this.available.get(opid).signal();

        } finally {
            this.mutex.unlock();
        }
    }

    /*
     * (non-Javadoc)
     * @see test.external.NodeLayer#doOp(operations.api.AbstractOperation,
     * alice.tucson.api.TucsonAgentId, alice.tucson.api.TucsonTupleCentreId)
     */
    public synchronized int doOp(final IOperation op, final TucsonAgentId aid,
            final TucsonTupleCentreId tci) throws TupleCentreNotFoundException {

        try {
            this.mutex.lock();

            this.mapping.put(this.opId, aid);
            Condition avail = this.mutex.newCondition();
            this.available.put(this.opId, avail);

            this.tcm.doOperation(op, aid, tci, this.opId);
            MyLogger.debug("Executing operation #" + this.opId + " for "
                    + aid.getAgentName());
            int res = this.opId;
            this.opId++;
            return res;

        } finally {
            this.mutex.unlock();
        }

    }
    
    @Override
    public synchronized IOperationResult getCurrStatus(final int opid) {
        try {
            this.mutex.lock();
            
            return this.tcm.getCurrStatus(opid);
            
        } finally {
            this.mutex.unlock();
        }
        
    }


    /*
     * (non-Javadoc)
     * @see test.external.NodeLayer#getResult(operations.api.AbstractOperation,
     * alice.tucson.api.TucsonAgentId, alice.tucson.api.TucsonTupleCentreId)
     */
    public IOperationResult getResult(final int opid) {
        try {
            this.mutex.lock();

            int opidentif = opid;

            /*
             * Looking for an operation for the agent NOT
            for (Entry<Integer, TucsonAgentId> entry : this.mapping.entrySet()) {
                if (entry.getValue() == aid) {
                    opidentif = entry.getKey();
                    break;
                }
            }
            */

            while (this.results.get(opidentif) == null) {
                try {
                    this.available.get(opidentif).await();
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            IOperationResult result = this.results.get(opidentif);
            MyLogger.debug("return result for op: " + opidentif);
            this.results.remove(opidentif);
            this.available.remove(opidentif);
            this.mapping.remove(opidentif);

            return result;

        } finally {
            this.mutex.unlock();
        }
    }

    
}
