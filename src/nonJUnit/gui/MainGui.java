/**
 * MainGui.java
 */
package nonJUnit.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import nonJUnit.tester.MainTester;

/**
 * @author Andrea Ridolfi (mailto: andrea.ridolfi5@studio.unibo.it) on
 *         25/lug/2014
 *
 */
public class MainGui {

    private JFrame frame;
    private MainTester nodo;
    private JTextArea textArea;

    /**
     * Create the application.
     * 
     * @param nodo
     *            the rest of the node
     */
    public MainGui(MainTester nodo2) {

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException
                | IllegalAccessException | UnsupportedLookAndFeelException e1) {
            e1.printStackTrace();
        }

        this.nodo = nodo2;
        initialize();

    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 700, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{684, 0};
        gridBagLayout.rowHeights = new int[]{562, 0};
        gridBagLayout.columnWeights = new double[]{0.0, 1.0};
        gridBagLayout.rowWeights = new double[]{0.0, Double.MIN_VALUE};
        frame.getContentPane().setLayout(gridBagLayout);
        
                JPanel mainpanel = new JPanel();
                GridBagConstraints gbc_mainpanel = new GridBagConstraints();
                gbc_mainpanel.fill = GridBagConstraints.BOTH;
                gbc_mainpanel.gridx = 0;
                gbc_mainpanel.gridy = 0;
                frame.getContentPane().add(mainpanel, gbc_mainpanel);
                GridBagLayout gbl_mainpanel = new GridBagLayout();
                gbl_mainpanel.columnWidths = new int[] { 0, 0, 0 };
                gbl_mainpanel.rowHeights = new int[] { 0, 0 };
                gbl_mainpanel.columnWeights =
                        new double[] { 0.4, 1.0, Double.MIN_VALUE };
                gbl_mainpanel.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
                mainpanel.setLayout(gbl_mainpanel);
                
                        JPanel panel_1 = new JPanel();
                        GridBagConstraints gbc_panel_1 = new GridBagConstraints();
                        gbc_panel_1.insets = new Insets(0, 0, 0, 5);
                        gbc_panel_1.fill = GridBagConstraints.BOTH;
                        gbc_panel_1.gridx = 0;
                        gbc_panel_1.gridy = 0;
                        mainpanel.add(panel_1, gbc_panel_1);
                        GridBagLayout gbl_panel_1 = new GridBagLayout();
                        gbl_panel_1.columnWidths = new int[] { 0, 0 };
                        gbl_panel_1.rowHeights = new int[] { 0, 0 };
                        gbl_panel_1.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
                        gbl_panel_1.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
                        panel_1.setLayout(gbl_panel_1);
                        
                                JPanel panel_2 = new JPanel();
                                GridBagConstraints gbc_panel_2 = new GridBagConstraints();
                                gbc_panel_2.fill = GridBagConstraints.BOTH;
                                gbc_panel_2.gridx = 0;
                                gbc_panel_2.gridy = 0;
                                panel_1.add(panel_2, gbc_panel_2);
                                GridBagLayout gbl_panel_2 = new GridBagLayout();
                                gbl_panel_2.columnWidths = new int[]{0, 0};
                                gbl_panel_2.rowHeights = new int[]{0, 0, 0, 0, 0};
                                gbl_panel_2.columnWeights = new double[]{1.0, 1.0};
                                gbl_panel_2.rowWeights = new double[]{1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
                                panel_2.setLayout(gbl_panel_2);
                                
                                JButton btnBottone1 = new JButton("Test 1");
                                btnBottone1.addActionListener(new ActionListener() {
                                    public void actionPerformed(ActionEvent arg0) {
                                    }
                                });
                                btnBottone1.addMouseListener(new MouseAdapter() {
                                    public void mouseClicked(MouseEvent arg0) {
                                        nodo.startFirstTest();
                                    }
                                });
                                GridBagConstraints gbc_btnBottone1 = new GridBagConstraints();
                                gbc_btnBottone1.fill = GridBagConstraints.BOTH;
                                gbc_btnBottone1.insets = new Insets(0, 0, 5, 5);
                                gbc_btnBottone1.gridx = 0;
                                gbc_btnBottone1.gridy = 0;
                                panel_2.add(btnBottone1, gbc_btnBottone1);
                                
                                JButton btnBottone2 = new JButton("Test2");
                                btnBottone2.addMouseListener(new MouseAdapter() {
                                    public void mouseClicked(MouseEvent arg0) {
                                        nodo.startSecondTest();
                                    }
                                });
                                
                                JTextArea txtrCreatesTasks = new JTextArea();
                                txtrCreatesTasks.setWrapStyleWord(true);
                                txtrCreatesTasks.setLineWrap(true);
                                txtrCreatesTasks.setEditable(false);
                                GridBagConstraints gbc_txtrCreatesTasks = new GridBagConstraints();
                                gbc_txtrCreatesTasks.insets = new Insets(0, 0, 5, 0);
                                gbc_txtrCreatesTasks.fill = GridBagConstraints.BOTH;
                                gbc_txtrCreatesTasks.gridx = 1;
                                gbc_txtrCreatesTasks.gridy = 0;
                                txtrCreatesTasks.setText("Creates 1000 tasks, each one insert in a random tuplecentre 1000 random tuple");
                                panel_2.add(txtrCreatesTasks, gbc_txtrCreatesTasks);
                                GridBagConstraints gbc_btnBottone2 = new GridBagConstraints();
                                gbc_btnBottone2.fill = GridBagConstraints.BOTH;
                                gbc_btnBottone2.insets = new Insets(0, 0, 5, 5);
                                gbc_btnBottone2.gridx = 0;
                                gbc_btnBottone2.gridy = 1;
                                panel_2.add(btnBottone2, gbc_btnBottone2);
                                
                                JButton btnBottone3 = new JButton("Test3");
                                btnBottone3.addMouseListener(new MouseAdapter() {
                                    public void mouseClicked(MouseEvent arg0) {
                                        nodo.startThirdTest();
                                    }
                                });
                                
                                JTextArea txtrSumsAllThe = new JTextArea();
                                txtrSumsAllThe.setText("Sums all the money on a tuplecentre");
                                txtrSumsAllThe.setLineWrap(true);
                                txtrSumsAllThe.setWrapStyleWord(true);
                                txtrSumsAllThe.setEditable(false);
                                GridBagConstraints gbc_txtrSumsAllThe = new GridBagConstraints();
                                gbc_txtrSumsAllThe.insets = new Insets(0, 0, 5, 0);
                                gbc_txtrSumsAllThe.fill = GridBagConstraints.BOTH;
                                gbc_txtrSumsAllThe.gridx = 1;
                                gbc_txtrSumsAllThe.gridy = 1;
                                panel_2.add(txtrSumsAllThe, gbc_txtrSumsAllThe);
                                GridBagConstraints gbc_btnBottone3 = new GridBagConstraints();
                                gbc_btnBottone3.fill = GridBagConstraints.BOTH;
                                gbc_btnBottone3.insets = new Insets(0, 0, 5, 5);
                                gbc_btnBottone3.gridx = 0;
                                gbc_btnBottone3.gridy = 2;
                                panel_2.add(btnBottone3, gbc_btnBottone3);
                                
                                JButton btnBottone4 = new JButton("Test4");
                                btnBottone4.addMouseListener(new MouseAdapter() {
                                    public void mouseClicked(MouseEvent arg0) {
                                        nodo.startForthTest();
                                    }
                                });
                                
                                JTextArea textArea_3 = new JTextArea();
                                textArea_3.setWrapStyleWord(true);
                                textArea_3.setEditable(false);
                                textArea_3.setLineWrap(true);
                                GridBagConstraints gbc_textArea_3 = new GridBagConstraints();
                                gbc_textArea_3.insets = new Insets(0, 0, 5, 0);
                                gbc_textArea_3.fill = GridBagConstraints.BOTH;
                                gbc_textArea_3.gridx = 1;
                                gbc_textArea_3.gridy = 2;
                                panel_2.add(textArea_3, gbc_textArea_3);
                                GridBagConstraints gbc_btnBottone4 = new GridBagConstraints();
                                gbc_btnBottone4.fill = GridBagConstraints.BOTH;
                                gbc_btnBottone4.insets = new Insets(0, 0, 0, 5);
                                gbc_btnBottone4.gridx = 0;
                                gbc_btnBottone4.gridy = 3;
                                panel_2.add(btnBottone4, gbc_btnBottone4);
                                
                                JTextArea textArea_4 = new JTextArea();
                                textArea_4.setWrapStyleWord(true);
                                textArea_4.setLineWrap(true);
                                textArea_4.setEditable(false);
                                GridBagConstraints gbc_textArea_4 = new GridBagConstraints();
                                gbc_textArea_4.fill = GridBagConstraints.BOTH;
                                gbc_textArea_4.gridx = 1;
                                gbc_textArea_4.gridy = 3;
                                panel_2.add(textArea_4, gbc_textArea_4);
                                
                                        textArea = new JTextArea();
                                        textArea.setEditable(false);
                                        textArea.setLineWrap(true);
                                        textArea.setWrapStyleWord(true);
                                        JScrollPane scrollPane = new JScrollPane();
                                        scrollPane.setViewportView(textArea);
                                        GridBagConstraints gbc_textArea = new GridBagConstraints();
                                        gbc_textArea.fill = GridBagConstraints.BOTH;
                                        gbc_textArea.gridx = 1;
                                        gbc_textArea.gridy = 0;
                                        mainpanel.add(scrollPane, gbc_textArea);

        frame.setVisible(true);

    }

    /**
     * @param string the string we update with the Gui
     */
    public void UpdateText(final String string) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                textArea.append(string + "\n");                
            }
        });
    }
}
