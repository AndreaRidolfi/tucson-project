/**
 * testbuffer.java
 */
package tuplespace.buffers;

import operations.impl.OpIn;
import alice.logictuple.LogicTuple;
import alice.tucson.api.TucsonAgentId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;

/**
 * @author Andrea Ridolfi (mailto: andrea.ridolfi5@studio.unibo.it) on
 *         30/mag/2014
 *
 */
public class TestTaskBuffer {

    static class MyThreadSetter extends Thread {

        private TaskBuffer buffer;

        public MyThreadSetter(final TaskBuffer buffer2) {
            this.buffer = buffer2;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    System.out.println("AddingTask..");
                    OpIn op = new OpIn(new LogicTuple("test!"));
                    TaskBlock task =
                            new TaskBlock(op, new TucsonAgentId("test"), null, 1);
                    this.buffer.putTask(task);
                    System.out.println("AddedTask "
                            + task.getOperation().toString());
                    Thread.sleep(1000);
                } catch (InterruptedException | TucsonInvalidAgentIdException e) {
                    e.printStackTrace();
                }

            }

        }
    }

    static class MyThreadGetter extends Thread {

        private TaskBuffer buffer;
        private String name;

        public MyThreadGetter(final TaskBuffer buffer2, final String name2) {
            this.buffer = buffer2;
            this.name = name2;
        }

        public void run() {
            while (true) {
                try {
                    System.out.println(name + " is RetrievingTask..");
                    TaskBlock task = this.buffer.getTask();
                    System.out.println("Retrieved by "
                            + name + " "
                            + task.getOperation().toString());
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

        }
    }

    /**
     * @param args main args (empty)
     */
    public static void main(final String[] args) {
        
        TaskBuffer buffer = new TaskBuffer();

        Thread a = new MyThreadGetter(buffer, "primo");
        Thread a2 = new MyThreadGetter(buffer, "secondo");
        Thread b = new MyThreadSetter(buffer);
        a.start();
        a2.start();
        b.start();

    }

}
