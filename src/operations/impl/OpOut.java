package operations.impl;

import java.util.List;

import operations.api.AbstractOperation;
import operations.api.IOperationResult;
import tuplespace.api.ITupleCentreOperations;
import alice.tuplecentre.api.Tuple;
import alice.tuplecentre.api.exceptions.InvalidOperationException;

/**
 * This is the OUT operation, which adds a tuple into the tuple centre
 * 
 * @author Andrea (mailto: andrea.ridolfi5@studio.unibo.it) on Apr 3, 2014
 * 
 */
public class OpOut extends AbstractOperation {
    /** serialVersionUID **/
    private static final long serialVersionUID = 1L;
    private final Tuple tuple;

    /**
     * @param tuple1
     *            construct the operation with the tuple
     */
    public OpOut(final Tuple tuple1) {
        this.tuple = tuple1;
    }

    /**
     * executes the operation on the tuplecentre tc
     * 
     * @param tc
     *            the tuplecentre where we want to perform the operation
     * @return the object return
     */
    @Override
    public IOperationResult execute(final ITupleCentreOperations tc) {
        try {
            final Tuple tupleres = tc.addTuple(this.tuple);
            final OperationResult opres = new OperationResult(this);
            opres.setResult(tupleres);
            return opres;
        } catch (final InvalidOperationException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Tuple getTuple() {
        return this.tuple;
    }

    /*
     * (non-Javadoc)
     * @see operations.api.IOperation#getTupleList()
     */
    @Override
    public List<Tuple> getTupleList() {
        return null;
    }
}
