/**
 * resultBuffer.java
 */
package tuplespace.buffers;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Andrea Ridolfi (mailto: andrea.ridolfi5@studio.unibo.it) on
 *         29/mag/2014
 * 
 */
public class ResultBuffer {
    private boolean available;
    private final List<ResultBlock> buffer;
    private final Condition isAvail;
    private final Lock mutex;

    /**
     * 
     */
    public ResultBuffer() {
        this.buffer = new LinkedList<ResultBlock>();
        this.mutex = new ReentrantLock();
        this.isAvail = this.mutex.newCondition();
        this.available = false;
    }

    public ResultBlock getResult() {
        try {
            this.mutex.lock();
            while (!this.available) {
                try {
                    this.isAvail.await();
                } catch (final InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
            final ResultBlock result = ((LinkedList<ResultBlock>) this.buffer).removeFirst();
            if (this.buffer.isEmpty()) {
                this.available = false;
            }
            return result;
        } finally {
            this.mutex.unlock();
        }
    }

    public void putResult(final ResultBlock result) {
        try {
            this.mutex.lock();
            this.buffer.add(result);
            this.available = true;
            this.isAvail.signal();
        } finally {
            this.mutex.unlock();
        }
    }
}
