/**
 * NodeLayer.java
 */
package nonJUnit.external;

import operations.api.IOperation;
import operations.api.IOperationResult;
import tuplespace.exceptions.TupleCentreNotFoundException;
import alice.tucson.api.TucsonAgentId;
import alice.tucson.api.TucsonTupleCentreId;

/**
 * @author Andrea Ridolfi (mailto: andrea.ridolfi5@studio.unibo.it) on Jul 31,
 *         2014
 *
 */
public interface IExternalLayer {

    /**
     * 
     * @param operationResult
     *            the result of the operation
     * @param aid
     *            the agent id requesting the operation
     * @param opid
     *            the operation identifier
     */
    void notifyResult(final IOperationResult operationResult,
            final TucsonAgentId aid, final int opid);

    /**
     * 
     * @param op
     *            operation we want performed
     * @param aid
     *            the agent requesting the operation
     * @param tci
     *            the tuple centre where we want to perform the operation
     * @throws TupleCentreNotFoundException
     *             if the tuplecentre is not existent
     * @return the unique identifier of the operation, used to retrieve
     *         information about its execution or the result
     */
    int doOp(final IOperation op, final TucsonAgentId aid,
            final TucsonTupleCentreId tci) throws TupleCentreNotFoundException;

    /**
     * ## THIS IS BLOCKING ## Blocking operation, we blocks the user who wants
     * to retrieve the operation until the node actively notifies that the
     * operation has been completed, giving us the OperationResult associated
     * 
     * @param opid
     *            the id of the operation we are interested in
     * @return the final result of the operation
     */
    IOperationResult getResult(int opid);

    /**
     * ## THIS IS NOT BLOCKING ## Gets the current status of the operation via a
     * non-blocking call of the TupleCentre Method getCurrStatus(int opId). This
     * is not the final operation result, but a still photograph of current
     * status of the operation.
     * 
     * 
     * @param opid
     *            the id of the operation we want to monitor
     * @return an IOperationResult of the current status of the operation
     */
    IOperationResult getCurrStatus(int opid);

}
