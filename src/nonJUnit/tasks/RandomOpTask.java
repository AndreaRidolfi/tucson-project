/**
 * RandomOpTask.java
 */
package nonJUnit.tasks;

import java.util.Random;

import nonJUnit.external.ExternalLayer;
import operations.api.IOperation;
import operations.api.IOperationResult;
import operations.api.OperationBuilder;
import alice.logictuple.LogicTuple;
import alice.logictuple.Value;
import alice.tucson.api.TucsonAgentId;
import alice.tucson.api.TucsonTupleCentreId;

/**
 * @author Andrea Ridolfi (mailto: andrea.ridolfi5@studio.unibo.it) on
 *         24/lug/2014
 *
 */
public class RandomOpTask implements Runnable {

    private Random rnd;
    private int maxn;
    private ExternalLayer nodo;
    private TucsonAgentId aid;

    public RandomOpTask(final int max, final ExternalLayer nodo2,
            final TucsonAgentId aid2) {
        this.rnd = new Random();
        this.maxn = max;
        this.nodo = nodo2;
        this.aid = aid2;

    }

    /*
     * (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    public void run() {

        try {
            int optype = this.rnd.nextInt(10);
            int number = this.rnd.nextInt(this.maxn);

            // Tuple >>> hello(world)
            LogicTuple tuple = new LogicTuple("hello", new Value(number));
            // Creates the Operation from the operation Builder

            IOperation op;
            if (optype == 0) {
                System.out.println(this.aid.getAgentName()
                        + " gonna wait for a read for a tuple " + tuple);
                op = OperationBuilder.getOpRdp(tuple);
            } else {
                if (optype == 1) {
                    System.out.println(this.aid.getAgentName()
                            + " gonna wait for a in for a tuple " + tuple);
                    op = OperationBuilder.getOpInp(tuple);
                } else {
                    op = OperationBuilder.getOpOut(tuple);
                }
            }

            // We identify the TupleCentre we want to do the operation in
            TucsonTupleCentreId tupleCentreId =
                    new TucsonTupleCentreId("default");
            // We launch the operation
            long t0 = System.currentTimeMillis();
            int opid = this.nodo.doOp(op, this.aid, tupleCentreId);
            long t1 = System.currentTimeMillis();
            System.out.println("Time elapsed: " + (t1 - t0) + "");
            // We retrieve the result
            IOperationResult res = this.nodo.getResult(opid);

            if (res.isFailed()) {
                System.out.println("[ERROR!]: " + res.getError());
            } else {

                if (optype == 0) {
                    System.out.println(this.aid.getAgentName() + " wrote: "
                            + res.getResult());
                } else {
                    if (optype == 1) {
                        System.out.println(this.aid.getAgentName()
                                + " removed: " + res.getResult());
                    } else {
                        System.out.println(this.aid.getAgentName() + " read: "
                                + res.getResult());
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
