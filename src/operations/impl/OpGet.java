package operations.impl;

import java.util.List;

import operations.api.AbstractOperation;
import operations.api.IOperationResult;
import tuplespace.api.ITupleCentreOperations;
import alice.tuplecentre.api.Tuple;

/**
 * This is the GET operation, which return all the tuples in the tuplecentre in a list
 * without removing them
 * 
 * @author Marco (mailto: marco.pari@studio.unibo.it) on July 03, 2014
 * 
 */
public class OpGet extends AbstractOperation {
    /** serialVersionUID **/
    private static final long serialVersionUID = 1L;

    /**
     * constructors
     */
    public OpGet() {
        super();
    }

    /*
     * (non-Javadoc)
     * @see
     * operations.api.IOperation#execute(tuplespace.api.ITupleCentreOperations)
     */
    @Override
    public IOperationResult execute(final ITupleCentreOperations tc) {
        List<Tuple> tuplelist = tc.getTupleList();
        final OperationResult opres = new OperationResult(this);
        opres.setResultList(tuplelist);
        return opres;
    }

    /*
     * (non-Javadoc)
     * @see operations.api.IOperation#getTuple()
     */
    @Override
    public Tuple getTuple() {
        return null;
    }

    /*
     * (non-Javadoc)
     * @see operations.api.IOperation#getTupleList()
     */
    @Override
    public List<Tuple> getTupleList() {
        return null;
    }
}
