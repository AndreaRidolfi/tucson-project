/**
 * TupleCentreAlreadyExists.java
 */
package tuplespace.exceptions;

/**
 * @author Rido (mailto: andrea.ridolfi5@studio.unibo.it) on 02/apr/2014
 * 
 */
public class TupleCentreAlreadyExistsException extends Exception {
    /** serialVersionUID **/
    private static final long serialVersionUID = 1L;
}
