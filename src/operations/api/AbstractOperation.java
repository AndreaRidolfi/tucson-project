/**
 * Operation.java
 */
package operations.api;

/**
 * 
 * A TuCSoN operation should only implement the execute() method
 * 
 * @author Andrea Ridolfi (mailto: andrea.ridolfi5@studio.unibo.it) on Apr 3,
 *         2014
 * 
 */
public abstract class AbstractOperation implements IOperation {
    /** serialVersionUID **/
    private static final long serialVersionUID = 1L;
    private boolean isCompleted = false;
    private boolean isPending = false;

    /*
     * (non-Javadoc)
     * @see operations.api.IOperation#isCompleted()
     */
    @Override
    public boolean isCompleted() {
        return this.isCompleted;
    }

    /*
     * (non-Javadoc)
     * @see operations.api.IOperation#setCompleted()
     */
    @Override
    public void setCompleted() {
        this.isPending = false;
        this.isCompleted = true;
    }

    /*
     * (non-Javadoc)
     * @see operations.api.IOperation#isPending()
     */
    public boolean isPending() {
        return this.isPending;
    }

    /*
     * (non-Javadoc)
     * @see operations.api.IOperation#setPending()
     */
    public void setPending() {
        this.isPending = true;
    }

}
