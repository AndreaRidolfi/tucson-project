/**
 * HelloWorlds.java
 */
package nonJUnit.simple;

import nonJUnit.external.ExternalLayer;
import operations.api.IOperation;
import operations.api.IOperationResult;
import operations.api.OperationBuilder;
import tuplespace.exceptions.TupleCentreNotFoundException;
import alice.logictuple.LogicTuple;
import alice.logictuple.Value;
import alice.tucson.api.TucsonAgentId;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;

/**
 * @author Andrea Ridolfi (mailto: andrea.ridolfi5@studio.unibo.it) on
 *         29/mag/2014
 * 
 */
public final class HelloWorlds {

    /**
     * 
     */
    private HelloWorlds() {

    }

    /**
     * New Test to try the new TuCSoN implementation
     * 
     * @param args
     *            args
     */
    public static void main(final String[] args) {

        /**
         * 
         * NOTE ON TEST: We create the MondoEsterno istance, which simply
         * creates our data structure without worrying about net problems etc.
         * 
         */

        try {
            // This is the istance which creates the node. just for test
            // purpose!
            ExternalLayer nodo = new ExternalLayer();

            // Our ID for the test
            TucsonAgentId aid = new TucsonAgentId("hello");

            // Tuple >>> hello(world)
            LogicTuple tuple = new LogicTuple("hello", new Value("world!"));
            // We identify the TupleCentre we want to do the operation in
            final TucsonTupleCentreId tupleCentreId =
                    new TucsonTupleCentreId("default");

            // Creates the Operation from the operation Builder
            IOperation op = OperationBuilder.getOpOut(tuple);
            // We launch the operation, saving the op identifier
            int opid = nodo.doOp(op, aid, tupleCentreId);
            // We retrieve the result using the opid provided
            IOperationResult res = nodo.getResult(opid);
            if (res.isFailed()) {
                System.out.println("[ERROR!]: " + res.getError());
            } else {
                System.out.println(res.getResult());
            }

            
            
//            // Creates the Operation from the operation Builder
//            op = OperationBuilder.getOpRead(tuple);
//            // We launch the operation
//            nodo.doOp(op, aid, tupleCentreId);
//            // We retrieve the result
//            res = nodo.getResult(aid);
//            if (res.isFailed()) {
//                System.out.println("[ERROR!]: " + res.getError());
//            } else {
//                System.out.println(res.getResult());
//            }
//            
//            
//            // Creates the Operation from the operation Builder
//            op = OperationBuilder.getOpIn(tuple);
//            // We launch the operation
//            nodo.doOp(op, aid, tupleCentreId);
//            // We retrieve the result
//            res = nodo.getResult(aid);
//            if (res.isFailed()) {
//                System.out.println("[ERROR!]: " + res.getError());
//            } else {
//                System.out.println(res.getResult());
//            }
//            
//            
//            
//            LogicTuple tuple2 = new LogicTuple("hello", new Var("X"));
//            // Creates the Operation from the operation Builder
//            IOperation op2 = new OpNop(tuple2);
//            // We launch the operation
//            nodo.doOp(op2, aid, tupleCentreId);
//            // We retrieve the result
//            IOperationResult res2 = nodo.getResult(aid);
//            if (res2.isFailed()) {
//                System.out.println("[ERROR!]: " + res2.getError());
//            } else {
//                System.out.println(res2.getResult());
//            }

    
            
        } catch (TupleCentreNotFoundException e) {
            e.printStackTrace();
        } catch (TucsonInvalidAgentIdException e) {
            e.printStackTrace();
        } catch (TucsonInvalidTupleCentreIdException e) {
            e.printStackTrace();
        }

    }
}
