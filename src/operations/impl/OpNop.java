package operations.impl;

import java.util.List;

import operations.api.AbstractOperation;
import operations.api.IOperationResult;
import tuplespace.api.ITupleCentreOperations;
import alice.logictuple.LogicTuple;
import alice.tuplecentre.api.Tuple;
import alice.tuplecentre.api.exceptions.InvalidOperationException;

/**
 * 
 * looks for a Tuple matching TupleTemplate in the target tuple space; if no
 * matching tuple is found in the target tuple space when the operation is
 * served, the execution succeeds, and TupleTemplate is returned; otherwise, if
 * a matching Tuple is found, the execution fails, and Tuple is returned
 * 
 * @author Marco (mailto: marco.pari@studio.unibo.it) on Jun 26, 2014
 * 
 */
public class OpNop extends AbstractOperation {
    /** serialVersionUID **/
    private static final long serialVersionUID = 1L;
    private final Tuple tuple;

    // THIS MUST BE CHANGED IN A TUPLETEMPLATE!
    /**
     * 
     * We want to read a tuple and NOT consuming it
     * 
     * @param tuple1
     *            the tuple we want to look for
     */
    public OpNop(final Tuple tuple1) {
        this.tuple = tuple1;
    }

    /**
     * @param tc
     *            the tuplecentre where we want to perform the operation
     * @return the object return
     */
    @Override
    public IOperationResult execute(final ITupleCentreOperations tc) {
        try {

            Tuple result = tc.readTuple(this.tuple);
            if (((LogicTuple) result).getName().equals("")) {
                final OperationResult opres = new OperationResult(this);
                opres.setResult(this.tuple);
                return opres;
            }
            final OperationResult opres = new OperationResult(this);
            opres.setResult(result);
            return opres;

        } catch (final InvalidOperationException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Tuple getTuple() {
        return this.tuple;
    }

    /*
     * (non-Javadoc)
     * @see operations.api.IOperation#getTupleList()
     */
    @Override
    public List<Tuple> getTupleList() {
        return null;
    }
}
