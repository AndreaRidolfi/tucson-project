package operations.api;

import java.util.List;

import operations.impl.OpGet;
import operations.impl.OpIn;
import operations.impl.OpInp;
import operations.impl.OpNo;
import operations.impl.OpNop;
import operations.impl.OpOut;
import operations.impl.OpRd;
import operations.impl.OpRdp;
import operations.impl.OpSet;
import alice.tuplecentre.api.Tuple;

/**
 * This class is to use for API, is simply build the Operation Objects. 
 * @author Andrea Ridolfi (mailto: andrea.ridolfi5@studio.unibo.it) on 10/apr/2014
 *
 */
public final class OperationBuilder {

    /**
     * 
     */
    private OperationBuilder() {
        //Useless constructor
    }
    /**
     * 
     * @return the operation
     */
    public static IOperation getOpget() {
        return new OpGet();
    }
    /**
     * 
     * @param tuple the tuple for the operation
     * @return the operation
     */
    public static IOperation getOpIn(final Tuple tuple) {
        return new OpIn(tuple);
    }
    /**
     * 
     * @param tuple the tuple for the operation
     * @return the operation
     */
    public static IOperation getOpInp(final Tuple tuple) {
        return new OpInp(tuple);
    }
    /**
     * 
     * @param tuple the tuple for the operation
     * @return the operation
     */
    public static IOperation getOpNo(final Tuple tuple) {
        return new OpNo(tuple);
    }
    /**
     * 
     * @param tuple the tuple for the operation
     * @return the operation
     */
    public static IOperation getOpNop(final Tuple tuple) {
        return new OpNop(tuple);
    }
    /**
     * 
     * @param tuple the tuple for the operation
     * @return the operation
     */
    public static IOperation getOpOut(final Tuple tuple) {
        return new OpOut(tuple);
    }
    /**
     * 
     * @param tuple the tuple for the operation
     * @return the operation
     */
    public static IOperation getOpRd(final Tuple tuple) {
        return new OpRd(tuple);
    }
    /**
     * 
     * @param tuple the tuple for the operation
     * @return the operation
     */
    public static IOperation getOpRdp(final Tuple tuple) {
        return new OpRdp(tuple);
    }
    /**
     * 
     * @param tuple the list of tuple for the operation
     * @return the operation
     */
    public static IOperation getOpSet(final List<Tuple> tuple) {
        return new OpSet(tuple);
    }
    
    
    
    
    
    
    

}
