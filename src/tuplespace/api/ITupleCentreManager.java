package tuplespace.api;

import operations.api.IOperation;
import operations.api.IOperationResult;
import tuplespace.exceptions.TupleCentreAlreadyExistsException;
import tuplespace.exceptions.TupleCentreNotFoundException;
import alice.tucson.api.TucsonAgentId;
import alice.tucson.api.TucsonTupleCentreId;

/**
 * 
 * @author Andrea (mailto: andrea.ridolfi5@studio.unibo.it) on Apr 3, 2014
 * 
 */
public interface ITupleCentreManager {
    /**
     * 
     * @param op
     *            the operation we want to perform
     * @param tci
     *            the tuplecentre where we want to perform the operation
     * @param aid
     *            AgentId
     * @param opId
     *            the operation identifier
     * @throws TupleCentreNotFoundException
     *             if the tuplecentre is not correct
     */
    void doOperation(IOperation op, TucsonAgentId aid,
            TucsonTupleCentreId tci, int opId)
            throws TupleCentreNotFoundException;
    
    /**
     * Creates a new TupleCentre in his space
     * 
     * @param tci
     *            creates a new TupleCentre with id tci
     * @throws TupleCentreAlreadyExistsException
     *             when we already have the same called tc in the space
     */
    void setUpTupleCentre(final TucsonTupleCentreId tci)
            throws TupleCentreAlreadyExistsException;

    /**
     * Removes a TupleCentre from his space
     * 
     * @param tci
     *            shuts down a new TupleCentre with id tci
     * @throws TupleCentreNotFoundException
     *            when there's no tc named tci in the space 
     */
    void shutDownTupleCentre(final TucsonTupleCentreId tci)
            throws TupleCentreNotFoundException;
    
    /**
     * 
     * @param opId the operation we want to know the current execution status
     * @return an object IOperationResult with the current operation status
     */
    IOperationResult getCurrStatus(int opId);
    
}
