/**
 * ResultNotifier.java
 */
package tuplespace.workers;

import nonJUnit.external.IExternalLayer;
import tuplespace.buffers.ResultBlock;
import tuplespace.buffers.ResultBuffer;

/**
 * @author Andrea Ridolfi (mailto: andrea.ridolfi5@studio.unibo.it) on 29/mag/2014
 *
 */
public class ResultNotifier extends Thread {
    
    private ResultBuffer buffer;
    private IExternalLayer mondo;
    
    /**
     * 
     * @param buffer1 buffer where we retrieve the result
     * @param layer the external world
     */
    public ResultNotifier(final ResultBuffer buffer1, final IExternalLayer layer) {
        this.buffer = buffer1;
        this.mondo = layer;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
        while (true) {
            ResultBlock result;
            result = this.buffer.getResult();
            this.mondo.notifyResult(result.getOperationResult(), result.getAgent(), result.getId());
            
        }
        
    }
    
    
}
