/**
 * ReaderTask.java
 */
package nonJUnit.tasks;

import nonJUnit.external.IExternalLayer;
import nonJUnit.gui.MainGui;
import nonJUnit.tester.DataGenerator;
import operations.api.IOperation;
import operations.api.IOperationResult;
import operations.api.OperationBuilder;
import alice.logictuple.LogicTuple;
import alice.tucson.api.TucsonAgentId;
import alice.tucson.api.TucsonTupleCentreId;

/**
 * @author Andrea Ridolfi (mailto: andrea.ridolfi5@studio.unibo.it) on
 *         25/lug/2014
 *
 */
public class WriterTask implements Runnable {
    private IExternalLayer nodo;
    private TucsonAgentId aid;
    private MainGui gui;
    private DataGenerator data;

    public WriterTask(final IExternalLayer nodo2,
            final TucsonAgentId aid2, final MainGui gui2) {
        this.data = new DataGenerator();
        this.nodo = nodo2;
        this.aid = aid2;
        this.gui = gui2;

    }

    /*
     * (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    public void run() {

        try {

            for (int i = 0; i < 100; i++) {
                
                
                LogicTuple tuple = this.data.getRandomInsertOperation();
                // Creates the Operation from the operation Builder

                IOperation op = OperationBuilder.getOpOut(tuple);

                // We identify the TupleCentre we want to do the operation in
                String tci = this.data.getRandomClient();
                TucsonTupleCentreId tupleCentreId =
                        new TucsonTupleCentreId(tci);
                
                // We retrieve the result
                IOperationResult res = this.nodo.getResult(this.nodo.doOp(op, this.aid, tupleCentreId));

                if (res.isFailed()) {
                    synchronized (System.out) {
                        System.out.println("[ERROR!]: " + res.getError());
                    }
                }
                
            }
            
            //At the end of the job we update the gui
            synchronized (System.out) {
                this.gui.UpdateText("[" + this.aid + "]:Job Finished!");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
