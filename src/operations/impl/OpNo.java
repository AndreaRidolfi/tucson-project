package operations.impl;

import java.util.List;

import operations.api.AbstractOperation;
import operations.api.IOperationResult;
import tuplespace.api.ITupleCentreOperations;
import alice.logictuple.LogicTuple;
import alice.tuplecentre.api.Tuple;
import alice.tuplecentre.api.exceptions.InvalidOperationException;

/**
 * 
 * looks for a Tuple matching TupleTemplate in the target tuple space; if no
 * matching tuple is found in the target tuple space when the operation is first
 * served, the execution succeeds, and TupleTemplate is returned; otherwise, the
 * execution is suspended, to be resumed and successfully completed when no
 * matching tuples can any longer be found in the target tuple space, then
 * TupleTemplate is returned
 * 
 * @author Marco (mailto: marco.pari@studio.unibo.it) on Jun 26, 2014
 * 
 */
public class OpNo extends AbstractOperation {
    /** serialVersionUID **/
    private static final long serialVersionUID = 1L;
    private final Tuple tuple;

    /**
     * 
     * @param tuple1
     *            the tuple we want to look for
     */
    public OpNo(final Tuple tuple1) {
        this.tuple = tuple1;
    }

    /**
     * @param tc
     *            the tuplecentre where we want to perform the operation
     * @return the object return
     */
    @Override
    public IOperationResult execute(final ITupleCentreOperations tc) {
        try {

            Tuple result = tc.readTuple(this.tuple);
            if (((LogicTuple) result).getName().equals("")) {
                final OperationResult opres = new OperationResult(this);
                opres.setResult(this.tuple);
                return opres;
            }
            final OperationResult opres = new OperationResult(this);
            opres.setError(this.tuple);
            return opres;

        } catch (final InvalidOperationException e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public Tuple getTuple() {
        return this.tuple;
    }

    /*
     * (non-Javadoc)
     * @see operations.api.IOperation#getTupleList()
     */
    @Override
    public List<Tuple> getTupleList() {
        return null;
    }
}
