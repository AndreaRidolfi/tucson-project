package tuplespace.api;

import tuplespace.buffers.TaskBlock;

/**
 * 
 * @author Andrea (mailto: andrea.ridolfi5@studio.unibo.it) on Apr 3, 2014
 * 
 */
public interface ITupleCentre {
    /**
     * 
     * @param task
     *            the operation
     */
    void doOperation(TaskBlock task);

    /**
     * ShutDown the tupleCentre
     */
    void shutDown();
}
