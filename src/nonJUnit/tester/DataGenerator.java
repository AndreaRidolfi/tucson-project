/**
 * DataGenerator.java
 */
package nonJUnit.tester;

import java.util.ArrayList;
import java.util.Random;

import alice.logictuple.LogicTuple;
import alice.logictuple.Value;

/**
 * @author Andrea Ridolfi (mailto: andrea.ridolfi5@studio.unibo.it) on Aug 1,
 *         2014
 * 
 *         L'idea � simulare il database di una banca. Ogni tuplecentre � un
 *         cliente, ogni tupla inserita � il tipo di transazione e il valore di
 *         quanto.
 * 
 */
public class DataGenerator {

    private Random rnd = new Random();
    private ArrayList<String> clients = new ArrayList<String>();
    private ArrayList<String> operation = new ArrayList<String>();

    public DataGenerator() {

        clients.add("andrea");
        clients.add("marco");
        clients.add("massimo");
        operation.add("versamento"); // ++
        operation.add("interessi"); // ++
        operation.add("stipendio"); // ++
        operation.add("prelievo"); // --
        operation.add("pagobancomat"); // --
        operation.add("fondo"); // --
        operation.add("bollo"); // --

    }

    /**
     * @return Restituisce a random un cliente su cui effettuare le operazioni
     */
    public String getRandomClient() {
        int index = rnd.nextInt(clients.size());
        return clients.get(index);

    }

    /**
     * Restituisce una tupla contentente una transazione casuale
     * 
     * @return la tupla
     */
    public LogicTuple getRandomInsertOperation() {

        // La transazione
        int index = rnd.nextInt(operation.size());
        String op = operation.get(index);

        // Il valore della transazione
        int number = 0;
        if (index < 3) {
            // Se � un operazione che aggiunge denaro
            number = rnd.nextInt(550);
        } else {
            // Se � un operazione che toglie denaro
            number = -(rnd.nextInt(400));
        }

        LogicTuple tuple = new LogicTuple(op, new Value(number));
        return tuple;
    }

}
