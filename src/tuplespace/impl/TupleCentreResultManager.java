/**
 * TupleCentreResultManager.java
 */
package tuplespace.impl;

import nonJUnit.external.IExternalLayer;
import operations.api.IOperation;
import operations.api.IOperationResult;
import tuplespace.api.ITupleCentreResultManager;
import tuplespace.buffers.ResultBlock;
import tuplespace.buffers.ResultBuffer;
import tuplespace.workers.ResultNotifier;
import alice.tucson.api.TucsonAgentId;

/**
 * @author Andrea Ridolfi (mailto: andrea.ridolfi5@studio.unibo.it) on
 *         29/mag/2014
 * 
 */
public class TupleCentreResultManager implements ITupleCentreResultManager {
    private final ResultBuffer buffer;

    public TupleCentreResultManager(final IExternalLayer layer) {
        this.buffer = new ResultBuffer();
        
     // Number of thread is ncore/2, if we have 1 core we create just one.
        int nthread = Runtime.getRuntime().availableProcessors() / 2;
        if (nthread == 0) {
            nthread++;
        }
        for (int i = 0; i < nthread; i++) {
            ResultNotifier thread = new ResultNotifier(this.buffer, layer);
            thread.start();
        }
        
    }

    @Override
    public void insertResult(final IOperationResult opres, final IOperation op,
            final TucsonAgentId agentId, final int opId) {
        this.buffer.putResult(new ResultBlock(opres, agentId, opId));
    }
}
