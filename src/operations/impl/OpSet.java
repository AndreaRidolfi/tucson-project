package operations.impl;

import java.util.List;

import operations.api.AbstractOperation;
import operations.api.IOperationResult;
import tuplespace.api.ITupleCentreOperations;
import alice.tuplecentre.api.Tuple;
import alice.tuplecentre.api.exceptions.InvalidOperationException;

/**
 * This is the SET operation, which change a set of tuples into the tuple centre
 * note: this will remove ALL the tuple already present in the given
 * tuplecentre, THEN it will add all the tuple of the list
 * 
 * @author Marco (mailto: marco.pari@studio.unibo.it) on July 03, 2014
 * 
 */
public class OpSet extends AbstractOperation {
    /** serialVersionUID **/
    private static final long serialVersionUID = 1L;
    private final List<Tuple> tuplelist;

    /**
     * @param tuplelist1
     *            construct the operation with the tuples
     */
    public OpSet(final List<Tuple> tuplelist1) {
        this.tuplelist = tuplelist1;
    }

    /*
     * (non-Javadoc)
     * @see
     * operations.api.IOperation#execute(tuplespace.api.ITupleCentreOperations)
     */
    @Override
    public IOperationResult execute(final ITupleCentreOperations tc) {
        tc.clearTupleSpace();
        for (final Tuple temp : this.tuplelist) {
            try {
                tc.addTuple(temp);
            } catch (final InvalidOperationException e) {
                e.printStackTrace();
            }
        }
        final OperationResult opres = new OperationResult(this);
        opres.setResultList(this.tuplelist);
        return opres;
    }

    /*
     * (non-Javadoc)
     * @see operations.api.IOperation#getTuple()
     */
    @Override
    public Tuple getTuple() {
        return null;
    }

    /*
     * (non-Javadoc)
     * @see operations.api.IOperation#getTupleList()
     */
    @Override
    public List<Tuple> getTupleList() {
        return this.tuplelist;
    }
}
