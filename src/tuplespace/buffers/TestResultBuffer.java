/**
 * testbuffer.java
 */
package tuplespace.buffers;

import operations.api.IOperation;
import operations.impl.OpIn;
import operations.impl.OperationResult;
import alice.logictuple.LogicTuple;
import alice.tucson.api.TucsonAgentId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;


/**
 * @author Andrea Ridolfi (mailto: andrea.ridolfi5@studio.unibo.it) on 30/mag/2014
 *
 */
public class TestResultBuffer {

    static class MyThreadSetter extends Thread {
        
        private ResultBuffer buffer;
        
        public MyThreadSetter(final ResultBuffer buffer2) {
            this.buffer = buffer2;
        }
        
        @Override
        public void run() {
            while (true) {
                try {
                    System.out.println("AddingResult..");
                    IOperation op = new OpIn(new LogicTuple("test"));
                    OperationResult result = new OperationResult(op);
                    result.setResult(new LogicTuple("result"));
                    ResultBlock resultBlock = new ResultBlock(result, new TucsonAgentId("test"), 1);
                    this.buffer.putResult(resultBlock);
                    System.out.println("AddedResult " + result.getResult());
                    Thread.sleep(1000);
                } catch (InterruptedException | TucsonInvalidAgentIdException e) {
                    e.printStackTrace();
                }
                
            }
            
        }               
    }
    
    static class MyThreadGetter extends Thread {
        
        private ResultBuffer buffer;
        
        public MyThreadGetter(final ResultBuffer buffer2) {
            this.buffer = buffer2;
        }
        
        @Override
        public void run() {
            while (true) {
                try {
                    System.out.println("RetrievingResult..");
                    ResultBlock result = this.buffer.getResult();
                    System.out.println("Retrieved " + result.getOperationResult());
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                
            }
            
        }               
    }
    /**
     * @param args main
     */
    public static void main(final String[] args) {
        ResultBuffer buffer = new ResultBuffer();
        
        Thread a = new MyThreadGetter(buffer);
        Thread a2 = new MyThreadGetter(buffer);
        Thread b = new MyThreadSetter(buffer);
        Thread b2 = new MyThreadSetter(buffer);
        Thread b3 = new MyThreadSetter(buffer);
        Thread b4 = new MyThreadSetter(buffer);
        Thread b5 = new MyThreadSetter(buffer);
        a.start();
        a2.start();
        b.start();
        b2.start();
        b3.start();
        b4.start();
        b5.start();

    }

}