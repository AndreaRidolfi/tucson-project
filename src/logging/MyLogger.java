/**
 * logger.java
 */
package logging;

/*
 * TODO rename, make logging level configurable (at run-time too)
 */
/**
 * @author Rido (mailto: andrea.ridolfi5@studio.unibo.it) on 02/apr/2014
 * 
 */
public final class MyLogger {
    private static boolean debugging = true;
    private static boolean logging = true;

    /**
     * 
     * @param ob
     *            the object we want to debug
     */
    public static void debug(final Object ob) {
        if (MyLogger.debugging) {
            System.out.println("[Debug]: " + ob);
        }
    }

    /**
     * 
     * @param ob
     *            the object we want to print
     */
    public static void log(final Object ob) {
        if (MyLogger.logging) {
            System.out.println("" + ob);
        }
    }

    /**
     * useless constructor for utility class
     */
    private MyLogger() {
        // never called
    }
}
