/**
 * ITupleCentreOperations.java
 */
package tuplespace.api;

import java.util.List;

import alice.tuplecentre.api.Tuple;
import alice.tuplecentre.api.exceptions.InvalidOperationException;

/**
 * @author Andrea Ridolfi (mailto: andrea.ridolfi5@studio.unibo.it) on
 *         03/lug/2014
 * 
 */
public interface ITupleCentreOperations {
    /**
     * 
     * @param tuple
     *            the tuple we want to insert into the tuplecentre
     * @return returns a message saying the tuple has been inserted
     * @throws InvalidOperationException
     *             invalid op
     */
    Tuple addTuple(Tuple tuple) throws InvalidOperationException;

    /**
     * Removes all the tuple from the space
     */
    void clearTupleSpace();

    /**
     * 
     * @return a List containing all the tuples inside the tuple space
     */
    List<Tuple> getTupleList();

    /**
     * This will only read the tuple inside the tuplecentre, leaving it there
     * 
     * @param tuple
     *            the tuple we want to read into the tuplecentre
     * @return returns a message saying the tuple has been read, with the tuple
     *         itself
     * @throws InvalidOperationException
     *             If the TuCSoN operation is invalid
     */
    Tuple readTuple(Tuple tuple) throws InvalidOperationException;

    /**
     * This will consume the tuple inside the tuplecentre
     * 
     * @param tuple
     *            the tuple we want to remove into the tuplecentre
     * @return returns a message saying the tuple has been removed
     * @throws InvalidOperationException
     *             getname
     */
    Tuple removeTuple(Tuple tuple) throws InvalidOperationException;
}
