/**
 * TupleSpaceTest.java
 */
package junit;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import nonJUnit.external.ExternalLayer;
import operations.api.IOperation;
import operations.api.IOperationResult;
import operations.api.OperationBuilder;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import tuplespace.exceptions.TupleCentreNotFoundException;
import alice.logictuple.LogicTuple;
import alice.logictuple.Value;
import alice.logictuple.Var;
import alice.tucson.api.TucsonAgentId;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tuplecentre.api.Tuple;

/**
 * @author Andrea Ridolfi (mailto: andrea.ridolfi5@studio.unibo.it) on Oct 14,
 *         2014
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TupleSpaceTest {

    private static ExternalLayer nodo;
    private static TucsonAgentId aid;
    private static TucsonTupleCentreId tci;
    private static int TUPLENUMBER = 10;

    /**
     * @throws java.lang.Exception
     *             ex
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        // This is the istance which creates the node. just for test
        // purpose!
        nodo = new ExternalLayer();
        aid = new TucsonAgentId("junit");
        tci = new TucsonTupleCentreId("testjunit");
    }

    /**
     * 
     * Also tests set operation
     * 
     * @throws java.lang.Exception
     *             ex
     */
    @Before
    public void setUp() throws Exception {
        List<Tuple> tuplelist = new ArrayList<Tuple>();
        for (int i = 0; i < TUPLENUMBER; i++) {
            LogicTuple tuple;
            if (i < TUPLENUMBER / 2) {
                tuple = new LogicTuple("hello", new Value("world" + i));
            } else {
                tuple = new LogicTuple("junitTest", new Value("tuple" + i));
            }
            tuplelist.add(tuple);
        }
        IOperation op = OperationBuilder.getOpSet(tuplelist);
        nodo.doOp(op, aid, tci);

    }

    /**
     * @throws java.lang.Exception
     *             ex
     */
    @After
    public void tearDown() throws Exception {
        /**
         * we don't have asave/exit procedure yet
         */
    }

    /**
     * Doing op in the first time finds the tuple After that the second
     * operation block the test, while a new thread write the tuple again in the
     * space After the thread succesfully write the tuple, the junit test
     * unblocks removing the tuple
     */
    @Test
    public void test2OpinOut() {
        try {
            jprint("TEST IN&OUT");

            jprint("1st operation in");
            LogicTuple tuple =
                    new LogicTuple("junitTest", new Value("tuple"
                            + (TUPLENUMBER - 1)));
            IOperation op = OperationBuilder.getOpIn(tuple);
            IOperationResult res = nodo.getResult(nodo.doOp(op, aid, tci));
            LogicTuple resulttuple = (LogicTuple) res.getResult();
            String resultstring = "junitTest(tuple" + (TUPLENUMBER - 1) + ")";
            assertTrue("Tuple removed is right",
                    resulttuple.toString().equals(resultstring));

            // Checking the tuple has been effectively removed
            IOperation opget = OperationBuilder.getOpget();
            IOperationResult resget =
                    nodo.getResult(nodo.doOp(opget, aid, tci));
            List<Tuple> tuplelist = resget.getResultList();
            assertTrue("Tuple Are " + (TUPLENUMBER - 1) + " has expected",
                    tuplelist.size() == (TUPLENUMBER - 1));

            // Doing again the operation, blocking waiting for the tuple
            int opid = nodo.doOp(op, aid, tci);

            jprint("Doing 2nd op in");
            // Starting a new Thread to write the tuple with a different aid
            new Thread(new Runnable() {
                public void run() {
                    try {
                        jprint("[THREAD]: Thread started, doing op out");
                        Thread.sleep(200);
                        TucsonAgentId aid2 = new TucsonAgentId("thread");
                        LogicTuple tuple2 =
                                new LogicTuple("junitTest", new Value("tuple"
                                        + (TUPLENUMBER - 1)));
                        IOperation op2 = OperationBuilder.getOpOut(tuple2);
                        nodo.doOp(op2, aid2, tci);

                    } catch (TucsonInvalidAgentIdException
                            | TupleCentreNotFoundException
                            | InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
            jprint("Retrieving op in");
            IOperationResult res2 = nodo.getResult(opid);
            LogicTuple resulttuple2 = (LogicTuple) res2.getResult();
            assertTrue("Tuple removed is right", resulttuple2.toString()
                    .equals(resultstring));

            // We check the tuple is missing again
            resget = nodo.getResult(nodo.doOp(opget, aid, tci));
            tuplelist = resget.getResultList();
            assertTrue("Tuple Are " + (TUPLENUMBER - 1) + " has expected",
                    tuplelist.size() == (TUPLENUMBER - 1));

        } catch (TupleCentreNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test3OpInp() {
        try {
            jprint("TEST INP");

            LogicTuple tupletemplate = new LogicTuple("hello", new Var("X"));
            IOperation op = OperationBuilder.getOpInp(tupletemplate);
            IOperationResult res = nodo.getResult(nodo.doOp(op, aid, tci));
            LogicTuple result = (LogicTuple) res.getResult();
            assertTrue("Tuple removed is right",
                    result.toString().equals("hello(world0)"));
            IOperation opget = OperationBuilder.getOpget();
            // Checking the tuple has been effectively removed
            IOperationResult resget =
                    nodo.getResult(nodo.doOp(opget, aid, tci));
            List<Tuple> tuplelist = resget.getResultList();
            assertTrue("Tuple Are " + (TUPLENUMBER - 1) + " has expected",
                    tuplelist.size() == (TUPLENUMBER - 1));

            tupletemplate = new LogicTuple("wrong", new Var("X"));
            op = OperationBuilder.getOpInp(tupletemplate);
            res = nodo.getResult(nodo.doOp(op, aid, tci));
            result = (LogicTuple) res.getResult();
            assertTrue("Tuple was not there, returned the template", result
                    .toString().equals("wrong(X)"));

        } catch (TupleCentreNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test4OpNo() {
        try {
            jprint("TEST NO");
            // This tuple is not present, so should return the template
            LogicTuple tupletemplate = new LogicTuple("wrong", new Var("X"));
            IOperation op = OperationBuilder.getOpNo(tupletemplate);
            IOperationResult res = nodo.getResult(nodo.doOp(op, aid, tci));
            LogicTuple result = (LogicTuple) res.getResult();
            assertTrue(
                    "Tuple is not present, should return the template itself",
                    result.toString().equals("wrong(X)"));
            jprint("ended first test");

            // We now write the tuple
            jprint("wrote tuple");
            LogicTuple tuple = new LogicTuple("wrong", new Value("test"));
            IOperation op2 = OperationBuilder.getOpOut(tuple);
            nodo.doOp(op2, aid, tci);

            // And we retry the operation
            jprint("retry the operation blocking");
            int opid = nodo.doOp(op, aid, tci);

            new Thread(new Runnable() {
                public void run() {
                    try {
                        jprint("[THREAD]: Thread started");
                        Thread.sleep(200);
                        TucsonAgentId aid2 = new TucsonAgentId("thread");
                        LogicTuple tupletemplate2 =
                                new LogicTuple("wrong", new Var("X"));
                        IOperation opthread =
                                OperationBuilder.getOpIn(tupletemplate2);
                        int opid2 = nodo.doOp(opthread, aid2, tci);
                        jprint("[THREAD]: Thread removed the tuple "
                                + nodo.getResult(opid2).getResult().toString());
                    } catch (TucsonInvalidAgentIdException
                            | TupleCentreNotFoundException
                            | InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
            res = nodo.getResult(opid);
            result = (LogicTuple) res.getResult();
            assertTrue(
                    "Tuple is present, should return the tupletemplate anyway",
                    result.toString().equals("wrong(X)"));

        } catch (TupleCentreNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test5OpNop() {
        try {
            jprint("TEST NOP");
            // This tuple is not present, so should return the template
            LogicTuple tupletemplate = new LogicTuple("wrong", new Var("X"));
            IOperation op = OperationBuilder.getOpNop(tupletemplate);
            IOperationResult res = nodo.getResult(nodo.doOp(op, aid, tci));
            LogicTuple result = (LogicTuple) res.getResult();
            assertTrue(
                    "Tuple is not present, should return the template itself",
                    result.toString().equals("wrong(X)"));

            // We now write the tuple
            LogicTuple tuple = new LogicTuple("wrong", new Value("test"));
            IOperation op2 = OperationBuilder.getOpOut(tuple);
            nodo.doOp(op2, aid, tci);

            // And we retry the operation
            jprint("retry the operation blocking");
            res = nodo.getResult(nodo.doOp(op, aid, tci));
            result = (LogicTuple) res.getResult();
            assertTrue("Tuple is present, should now return the tuple", result
                    .toString().equals("wrong(test)"));

        } catch (TupleCentreNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void test6OpGet() {
        try {
            jprint("TEST GET");

            IOperation opget = OperationBuilder.getOpget();
            IOperationResult resget =
                    nodo.getResult(nodo.doOp(opget, aid, tci));
            List<Tuple> tuplelist = resget.getResultList();
            assertTrue("Tuple Are " + TUPLENUMBER + " has expected",
                    tuplelist.size() == TUPLENUMBER);

            IOperation op2 =
                    OperationBuilder.getOpOut(new LogicTuple("junitTest",
                            new Value("tuple" + 100)));
            nodo.doOp(op2, aid, tci);

            resget = nodo.getResult(nodo.doOp(opget, aid, tci));
            tuplelist = resget.getResultList();
            assertTrue("Tuple Are " + (TUPLENUMBER + 1) + " has expected",
                    tuplelist.size() == TUPLENUMBER + 1);

        } catch (TupleCentreNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test7OpRd() {
        try {
            jprint("TEST RD");

            LogicTuple tupletemplate = new LogicTuple("hello", new Var("X"));
            IOperation op = OperationBuilder.getOpRd(tupletemplate);
            IOperationResult res = nodo.getResult(nodo.doOp(op, aid, tci));
            LogicTuple result = (LogicTuple) res.getResult();
            assertTrue("Tuple read is right",
                    result.toString().equals("hello(world0)"));

            // Checking the tuple has not been removed
            IOperation opget = OperationBuilder.getOpget();
            IOperationResult resget =
                    nodo.getResult(nodo.doOp(opget, aid, tci));
            List<Tuple> tuplelist = resget.getResultList();
            assertTrue("Tuple Are " + (TUPLENUMBER) + " has expected",
                    tuplelist.size() == (TUPLENUMBER));

            // We now block reading a tuple which is not present
            tupletemplate = new LogicTuple("wrong", new Var("X"));
            op = OperationBuilder.getOpRd(tupletemplate);
            int opid = nodo.doOp(op, aid, tci);

            // Adding the tuple with a different Agent
            new Thread(new Runnable() {
                public void run() {
                    try {
                        jprint("[THREAD]: Thread started, doing op out");
                        Thread.sleep(200);
                        TucsonAgentId aid2 = new TucsonAgentId("thread");

                        LogicTuple tuple2 =
                                new LogicTuple("wrong", new Value("tuple"));
                        IOperation op2 = OperationBuilder.getOpOut(tuple2);
                        nodo.doOp(op2, aid2, tci);
                        jprint("[THREAD]: Thread Finished");
                    } catch (TucsonInvalidAgentIdException
                            | TupleCentreNotFoundException
                            | InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
            // Retrieve result
            res = nodo.getResult(opid);
            result = (LogicTuple) res.getResult();
            assertTrue("Tuple was not there, returned the template", result
                    .toString().equals("wrong(tuple)"));

            // Tuples should be now TUPLENUMBER+1
            resget = nodo.getResult(nodo.doOp(opget, aid, tci));
            tuplelist = resget.getResultList();
            assertTrue("Tuple Are " + (TUPLENUMBER + 1) + " has expected",
                    tuplelist.size() == (TUPLENUMBER + 1));

        } catch (TupleCentreNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test8OpRdp() {
        try {
            jprint("TEST RDP");

            LogicTuple tupletemplate = new LogicTuple("hello", new Var("X"));
            IOperation op = OperationBuilder.getOpRdp(tupletemplate);
            IOperationResult res = nodo.getResult(nodo.doOp(op, aid, tci));
            LogicTuple result = (LogicTuple) res.getResult();
            assertTrue("Tuple read is right",
                    result.toString().equals("hello(world0)"));
            IOperation opget = OperationBuilder.getOpget();
            // Checking the tuple has not been removed
            IOperationResult resget =
                    nodo.getResult(nodo.doOp(opget, aid, tci));
            List<Tuple> tuplelist = resget.getResultList();
            assertTrue("Tuple Are " + (TUPLENUMBER) + " has expected",
                    tuplelist.size() == (TUPLENUMBER));

            tupletemplate = new LogicTuple("wrong", new Var("X"));
            op = OperationBuilder.getOpRdp(tupletemplate);
            res = nodo.getResult(nodo.doOp(op, aid, tci));
            result = (LogicTuple) res.getResult();
            assertTrue("Tuple was not there, returned the template", result
                    .toString().equals("wrong(X)"));

        } catch (TupleCentreNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test9OpStatus() {
        try {
            jprint("TESTING OPERATION STATUS");

            LogicTuple tuple =
                    new LogicTuple("junitTest", new Value("statusTest"));

            IOperation op = OperationBuilder.getOpIn(tuple);
            jprint("doing in operation");
            int opid = nodo.doOp(op, aid, tci);

            Thread.sleep(200); // need the sleep since it's not synchronized so
                              // the operation can be pending while we ask to
                              // early
            IOperationResult res = nodo.getCurrStatus(opid);

            assertTrue("TupleOperation is pending", res.getOperation()
                    .isPending());
            
            IOperation op2 = OperationBuilder.getOpOut(tuple);
            jprint("doing out operation");
            nodo.doOp(op2, aid, tci);
            
            //Now the operation should be completed
            Thread.sleep(200);
            res = nodo.getCurrStatus(opid);
            assertTrue("TupleOperation is no more pending", !res.getOperation()
                    .isPending());
            assertTrue("TupleOperation is completed", res.getOperation()
                    .isCompleted());
            
            
            
            jprint("done");
        } catch (TupleCentreNotFoundException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void jprint(final Object o) {
        System.out.println("[JUNIT]: " + o);
    }

}
