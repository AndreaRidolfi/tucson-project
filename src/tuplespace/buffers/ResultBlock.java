/**
 * TaskBlock.java
 */
package tuplespace.buffers;

import java.io.Serializable;

import operations.api.IOperationResult;
import alice.tucson.api.TucsonAgentId;

/**
 * 
 * Data struct we use for storing the result of any TuCSoN operation in the
 * buffer Actually contains just the OperationResult and the TuCSoN Agent ID of
 * the agent who request the operation
 * 
 * @author Andrea Ridolfi (mailto: andrea.ridolfi5@studio.unibo.it) on
 *         29/mag/2014
 * 
 */
public class ResultBlock implements Serializable {
    /** serialVersionUID **/
    private static final long serialVersionUID = 1L;
    
    private final TucsonAgentId agentId;
    private final IOperationResult opres;
    private final int resultId;

    /**
     * @param opres1
     *            the operation we want to perform on the tid
     * @param aid
     *            The agent who requested the operation
     * @param resultId2
     *            unique identifier for the operation
     */
    public ResultBlock(final IOperationResult opres1, final TucsonAgentId aid,
            final int resultId2) {
        this.opres = opres1;
        this.agentId = aid;
        this.resultId = resultId2;
    }

    /**
     * 
     * @return the agent who requested the operetion
     */
    public TucsonAgentId getAgent() {
        return this.agentId;
    }

    /**
     * 
     * @return the result id (which is the same of the operation id)
     */
    public int getId() {
        return this.resultId;
    }

    /**
     * 
     * @return the result of the operation performed
     */
    public IOperationResult getOperationResult() {
        return this.opres;
    }
    
}
