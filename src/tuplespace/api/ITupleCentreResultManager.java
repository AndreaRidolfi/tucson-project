/**
 * ITupleCentreResultManager.java
 */
package tuplespace.api;

import operations.api.IOperation;
import operations.api.IOperationResult;
import alice.tucson.api.TucsonAgentId;

/**
 * @author Andrea Ridolfi (mailto: andrea.ridolfi5@studio.unibo.it) on
 *         30/mag/2014
 * 
 */
public interface ITupleCentreResultManager {
    /**
     * 
     * @param opres
     *            result to be inserted
     * @param op 
     *            the original operation
     * @param agentId
     *            Id of the agent who requested the operation
     * @param opId
     *            An unique operation identifier
     */
    void insertResult(IOperationResult opres, IOperation op, TucsonAgentId agentId, int opId);

}
